## Build Instructions

To protect the API key and the signing credentials, you'll need two additional
files. Please **send me an email** (**remco.janssen@altran.nl**) if you actually want
to build the app with valid api key or a release build.

App can be buld using standard android gradle commands

`gradlew assembleRelease` *

\* Not possible without sign.properties

`gradlew assembleDebug` **
    
\** Will use a fake API key, so the API will never return results


### Details
We use a file `sign.properties` which contains sensitive project data. This file is not committed to the repo for security considerations. Upon building, properties from this file will be put into `BuildConfig.java` class by gradle

**Release Builds**

* are obfuscated using Proguard.
* are +- 1.72Mb
* don't log


### Future Work
For future sprints, I would suggest to move the entire build process to Jenkins. This would include automatic versioning and archiving of the builds. Unfortunatly, I don't have a private cloud with a Jenkins installation for projects like these ;)