package nl.entreco.pinsandroid.detail;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.view.View;

import java.util.Locale;

import nl.entreco.pinsandroid.BaseApplication;
import nl.entreco.pinsandroid.ViewModel;
import nl.entreco.pinsandroid.api.details.FlickrDetailedService;
import nl.entreco.pinsandroid.search.Photo;
import nl.entreco.pinsandroid.utils.Utils;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * ViewModel for the DetailedActivity
 */
@SuppressWarnings("WeakerAccess")
public class DetailedViewModel extends BaseObservable implements ViewModel {

    private static final String TAG = "DetailedViewModel";

    private final Photo photo;
    private final DataListener datalistener;

    private Context context;
    private Subscription subscription;

    public final ObservableField<String> photoDescription;
    public final ObservableField<String> datePosted;
    public final ObservableField<String> dateTaken;
    public final ObservableField<String> ownerRealName;
    public final ObservableInt numberOfViews;
    public final ObservableInt hasDescription;

    public DetailedViewModel(Context _context, final Photo _photo, final DataListener _dataListener) {
        this.photo = _photo;
        this.context = _context;
        this.datalistener = _dataListener;
        this.photoDescription = new ObservableField<>();
        this.datePosted = new ObservableField<>();
        this.dateTaken = new ObservableField<>();
        this.ownerRealName = new ObservableField<>();
        this.numberOfViews = new ObservableInt(0);
        this.hasDescription = new ObservableInt(View.GONE);
        // Trigger loading the rest of the user data as soon as the view model is created.
        // It's odd having to trigger this from here. Cases where accessing to the data model
        // needs to happen because of a change in the Activity/Fragment lifecycle
        // (i.e. an activity created) don't work very well with this MVVM pattern.
        // It also makes this class more difficult to test. Hopefully a better solution will be found
        loadDetails(_photo);
    }

    public String getPhotoTitle() {
        return photo.title;
    }

    public String getBigUrl() {
        return Utils.bigImageUrlForPhoto(photo);
    }

    public String getTransitionName(){ return String.format(Locale.getDefault(), "remco_%d", photo.id);}

    @Override
    public void destroy() {
        this.context = null;
        if (subscription != null && !subscription.isUnsubscribed()) subscription.unsubscribe();
    }

    private void loadDetails(final Photo _photo) {
        if(Utils.isConnected(context)) {
            BaseApplication application = BaseApplication.get(context);
            FlickrDetailedService flickrService = application.getFlickrDetailedService();
            subscription = flickrService.getPhotoInfo(String.valueOf(_photo.id))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(application.defaultSubscribeScheduler())
                    .subscribe(new Action1<DetailedResult>() {

                        @Override
                        public void call(final DetailedResult _info) {
                            photoDescription.set(_info.getDescription());
                            ownerRealName.set(_info.getOwnerName());
                            datePosted.set(_info.getDatePosted());
                            dateTaken.set(_info.getDateTaken());
                            numberOfViews.set(_info.getNumberOfViews());
                            hasDescription.set(_info.getDescription().isEmpty() ? View.GONE : View.VISIBLE);
                        }
                    });
        } else if(this.datalistener != null){
            this.datalistener.onOffline();
        }
    }

    public void retry() {
        loadDetails(this.photo);
    }

    public interface DataListener{
        void onOffline();
    }
}
