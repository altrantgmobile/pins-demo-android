package nl.entreco.pinsandroid.api;

import java.util.List;

/**
 * Created by 310185349 on 18-7-2016.
 */
public interface FlickrParams {
    List<FlickrRequestParams.Param> getParameters();
}
