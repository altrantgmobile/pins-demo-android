package nl.entreco.pinsandroid.api;

import nl.entreco.pinsandroid.BuildConfig;

/**
 * Created by 310185349 on 19-7-2016.
 */
public abstract class FlickrRequestParams implements FlickrParams {

    protected static final String SEARCH = "flickr.photos.search";
    protected static final String INFO = "flickr.photos.getInfo";
    protected static final String API_KEY = BuildConfig.API_KEY;
    protected static final String CONTENT_TYPE = "1";
    public static final int PHOTOS_PER_PAGE = 50;

    public static class Param {
        final String name;
        final String value;
        public Param(String _name, String _value){
            name = _name;
            value = _value;
        }
        public String getName(){return name;}
        public String getValue(){return value;}
    }
}
