package nl.entreco.pinsandroid.detail;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import nl.entreco.pinsandroid.R;
import nl.entreco.pinsandroid.databinding.DetailsActivityBinding;
import nl.entreco.pinsandroid.search.Photo;

public class DetailedActivity extends AppCompatActivity implements DetailedViewModel.DataListener {

    private static final String EXTRA_PHOTO = "EXTRA_PHOTO";

    private DetailsActivityBinding binding;
    private DetailedViewModel mDetailedViewModel;

    public static Intent newIntent(Context context, Photo photo) {
        Intent intent = new Intent(context, DetailedActivity.class);
        intent.putExtra(EXTRA_PHOTO, photo);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.details_activity);
        setSupportActionBar(binding.includeToolbar.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Photo photo = getIntent().getParcelableExtra(EXTRA_PHOTO);
        mDetailedViewModel = new DetailedViewModel(this, photo, this);
        binding.setViewModel(mDetailedViewModel);

        // Clear title
        setTitle("");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDetailedViewModel.destroy();
    }

    @Override
    public void onOffline() {
        final Snackbar snackbar = Snackbar.make(binding.getRoot(), R.string.error_offline, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.retry, new View.OnClickListener() {
            @Override
            public void onClick(final View _view) {
                mDetailedViewModel.retry();
                snackbar.dismiss();
            }
        });
        snackbar.getView().setBackgroundResource(R.color.snackbar_bg);
        snackbar.show();
    }
}
