package nl.entreco.pinsandroid.search.items;

import android.content.Context;
import android.databinding.BaseObservable;
import android.view.View;

import java.util.Locale;

import nl.entreco.pinsandroid.ViewModel;
import nl.entreco.pinsandroid.search.Photo;
import nl.entreco.pinsandroid.utils.Utils;

/**
 * View model for each Photo RecyclerView
 */
public class SearchItemViewModel extends BaseObservable implements ViewModel {

    private final DataListener dataListener;
    private Photo photo;
    private Context context;

    public SearchItemViewModel(Context context, Photo photo, DataListener _datalistener) {
        this.photo = photo;
        this.context = context;
        this.dataListener = _datalistener;
    }

    public String getTitle() {
        return photo.title;
    }

    public String getThumb() {
        return Utils.thumbImageUrlForPhoto(photo);
    }

    public String getTransitionName() {
        return String.format(Locale.getDefault(), "remco_%d", photo.id);
    }

    public void onItemClick(View _view) {
        Utils.hideKeyboard(_view);
        if (this.dataListener != null) {
            dataListener.onItemClicked(_view, photo);
        }
    }

    // Allows recycling ItemRepoViewModels within the recyclerview adapter
    public void setPhoto(Photo _photo) {
        this.photo = _photo;
        notifyChange();
    }

    @Override
    public void destroy() {
        //In this case destroy doesn't need to do anything because there is not async calls
        this.context = null;
    }

    public interface DataListener {
        void onItemClicked(final View _view, final Photo _photo);
    }
}
