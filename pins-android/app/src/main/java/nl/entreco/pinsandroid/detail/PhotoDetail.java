package nl.entreco.pinsandroid.detail;

import com.google.gson.annotations.SerializedName;

/**
 * Created by 310185349 on 18-7-2016.
 */
@SuppressWarnings("CanBeFinal")
public class PhotoDetail {
    @SerializedName("id")
    private String id;
    @SerializedName("secret")
    private String secret;
    @SerializedName("server")
    private String server;
    @SerializedName("farm")
    public int farm;
    @SerializedName("dateuploaded")
    private String dateUploaded;
    @SerializedName("originalformat")
    private String originalFormat;
    @SerializedName("owner")
    public PhotoOwner owner;
    @SerializedName("description")
    public PhotoDescription description;
    @SerializedName("visibility")
    private PhotoVisibility visibility;
    @SerializedName("dates")
    public PhotoDates dates;
    @SerializedName("views")
    int views;

    public PhotoDetail(){
        id = "";
        secret = "";
        server = "";
        dateUploaded = "";
        originalFormat = "";
        owner = new PhotoOwner();
        description = new PhotoDescription();
        visibility = new PhotoVisibility();
    }

    static class PhotoOwner{
        @SerializedName("nsid")
        String id;
        @SerializedName("username")
        String username;
        @SerializedName("realname")
        String realname;
        @SerializedName("location")
        String location;
        PhotoOwner(){
            id = "";
            username = "";
            realname = "";
            location = "";
        }
    }

    static class PhotoDescription{
        @SerializedName("_content")
        String content;
        PhotoDescription(){
            content = "";
        }
    }

    static class PhotoVisibility{
        @SerializedName("isPublic")
        boolean isPublic;
        @SerializedName("isFriend")
        boolean isFriend;
        @SerializedName("isFamily")
        boolean isFamily;
    }

    static class PhotoDates{
        @SerializedName("posted")
        String posted;
        @SerializedName("taken")
        String taken;
    }
}
