package nl.entreco.pinsandroid.api.details;

import java.util.ArrayList;
import java.util.List;

import nl.entreco.pinsandroid.api.FlickrRequestParams;

/**
 * Created by 310185349 on 18-7-2016.
 */
public final class FlickrDetailedRequest extends FlickrRequestParams {

    public static final List<Param> sParams = new ArrayList<Param>() {
        {
            add(new Param("method", INFO));
            add(new Param("api_key", API_KEY));
            add(new Param("format", "json"));
            add(new Param("nojsoncallback", "1"));
        }
    };

    @Override
    public List<Param> getParameters() {
        return sParams;
    }
}
