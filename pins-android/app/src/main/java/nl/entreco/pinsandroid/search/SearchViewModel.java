package nl.entreco.pinsandroid.search;

import android.content.Context;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import nl.entreco.pinsandroid.BaseApplication;
import nl.entreco.pinsandroid.R;
import nl.entreco.pinsandroid.ViewModel;
import nl.entreco.pinsandroid.api.search.FlickrSearchService;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * View model for the SearchActivity
 */
public class SearchViewModel implements ViewModel {

    private static final String TAG = "SearchViewModel";

    public final ObservableInt infoMessageVisibility;
    public final ObservableInt progressVisibility;
    public final ObservableInt recyclerViewVisibility;
    public final ObservableInt searchButtonVisibility;
    private final DataListener dataListener;

    public final ObservableField<String> infoMessage;
    private Context context;
    private Subscription subscription;
    private SearchResult mSearchResult;
    private String queryText;

    public SearchViewModel(Context context, DataListener dataListener) {
        this.context = context;
        this.dataListener = dataListener;
        infoMessageVisibility = new ObservableInt(View.VISIBLE);
        progressVisibility = new ObservableInt(View.INVISIBLE);
        recyclerViewVisibility = new ObservableInt(View.INVISIBLE);
        searchButtonVisibility = new ObservableInt(View.GONE);
        infoMessage = new ObservableField<>(context.getString(R.string.default_info_message));
    }

    @Override
    public void destroy() {
        if (subscription != null && !subscription.isUnsubscribed()) subscription.unsubscribe();
        subscription = null;
        context = null;
    }

    public boolean onSearchAction(TextView view, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            String searchText = view.getText().toString();
            if (searchText.length() > 0) searchPhotos(searchText, 0);
            return true;
        }
        return false;
    }

    public void onClickSearch(View view) {
        searchPhotos(queryText, 0);
    }

    public TextWatcher getSearchQueryWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                queryText = charSequence.toString();
                searchButtonVisibility.set(charSequence.length() > 0 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
    }

    private void searchPhotos(final String searchString, final int page) {

        if (page == 0) {
            clearPhotos();
            progressVisibility.set(View.VISIBLE);
            recyclerViewVisibility.set(View.INVISIBLE);
            infoMessageVisibility.set(View.INVISIBLE);
        }

        if (subscription != null && !subscription.isUnsubscribed()) subscription.unsubscribe();
        BaseApplication application = BaseApplication.get(context);
        FlickrSearchService flickrSearchService = application.getFlickrSearchService();


        subscription = flickrSearchService.performTextSearch(searchString, page)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.defaultSubscribeScheduler())
                .subscribe(new Subscriber<SearchResult>() {
                    @Override
                    public void onCompleted() {
                        if (dataListener != null) dataListener.onPhotosLoaded(mSearchResult);
                        progressVisibility.set(View.INVISIBLE);
                        if (!mSearchResult.isEmpty()) {
                            recyclerViewVisibility.set(View.VISIBLE);
                        } else {
                            infoMessage.set(context.getString(R.string.no_photos_found, searchString));
                            infoMessageVisibility.set(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onError(final Throwable error) {
                        Log.e(TAG, "Error searching pics ", error);
                        progressVisibility.set(View.INVISIBLE);
                        infoMessage.set(context.getString(R.string.error_loading_repos));
                        infoMessageVisibility.set(View.VISIBLE);
                    }

                    @Override
                    public void onNext(final SearchResult _searchResult) {
                        Log.i(TAG, "SearchResult loaded " + _searchResult);
                        SearchViewModel.this.mSearchResult = _searchResult;
                        SearchViewModel.this.mSearchResult.photos.keyword = searchString;
                    }
                });
    }

    private void clearPhotos() {
        this.mSearchResult = null;
        if (dataListener != null) {
            dataListener.onPhotosLoaded(null);
        }
    }

    public void loadMore() {
        if (this.mSearchResult != null) {
            searchPhotos(this.mSearchResult.photos.keyword, this.mSearchResult.photos.page + 1);
        }
    }

    public interface DataListener {
        void onPhotosLoaded(SearchResult _searchResult);
    }
}
