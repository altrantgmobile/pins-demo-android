package nl.entreco.pinsandroid.search;

import android.app.ActivityOptions;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.EditText;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import nl.entreco.pinsandroid.R;
import nl.entreco.pinsandroid.databinding.SearchActivityBinding;
import nl.entreco.pinsandroid.detail.DetailedActivity;
import nl.entreco.pinsandroid.search.items.SearchAdapter;
import nl.entreco.pinsandroid.utils.Utils;

public class SearchActivity extends AppCompatActivity implements SearchViewModel.DataListener, SearchAdapter.AdapterCallback {

    private SearchActivityBinding binding;
    private SearchViewModel mSearchViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.search_activity);
        mSearchViewModel = new SearchViewModel(this, this);
        binding.setViewModel(mSearchViewModel);
        setSupportActionBar(binding.includeToolbar.toolbar);
        setupRecyclerView(binding.includeContent.recyclerView);
        setupAppBarListener();
    }

    @Override
    protected void onPause() {
        Utils.hideKeyboard(getEditText());
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mSearchViewModel.destroy();
        super.onDestroy();
    }

    @Override
    public void onPhotosLoaded(final SearchResult _searchResult) {
        SearchAdapter adapter =
                (SearchAdapter) binding.includeContent.recyclerView.getAdapter();
        adapter.addPhotos(_searchResult);
        Utils.hideKeyboard(getEditText());
    }

    @Override
    public void almostAtTheEnd() {
        mSearchViewModel.loadMore();
        final Snackbar snackbar = Snackbar.make(binding.getRoot(), R.string.loading_items, Snackbar.LENGTH_SHORT);
        snackbar.getView().setBackgroundResource(R.color.snackbar_bg);
        snackbar.show();
    }

    @Override
    public void onItemClick(final View _view, final Photo _photo) {
        ActivityOptions opts = ActivityOptions.makeSceneTransitionAnimation(this);
        startActivity(DetailedActivity.newIntent(this, _photo), opts.toBundle());
    }

    private void setupRecyclerView(RecyclerView _recyclerView) {
        SearchAdapter adapter = new SearchAdapter(this);
        _recyclerView.setAdapter(adapter);
        _recyclerView.setLayoutManager(Utils.orientationAwareGridLayoutManager(this));
        _recyclerView.setItemAnimator(new SlideInUpAnimator(new OvershootInterpolator(1f)));
        _recyclerView.getItemAnimator().setAddDuration(250);
    }

    private void setupAppBarListener() {
        getAppbar().addOnOffsetChangedListener(new AppBarStateChangeListener() {
            @Override
            public void onStateChanged(final AppBarLayout appBarLayout, final State state) {
                if (state.equals(State.EXPANDED)) {
                    getEditText().selectAll();
                    Utils.showKeyboard(getEditText());
                } else {
                    Utils.hideKeyboard(getEditText());
                }
            }
        });
    }

    private AppBarLayout getAppbar() {
        return binding.includeToolbar.appBarLayout;
    }

    private EditText getEditText() {
        return binding.includeToolbar.searchQueryInput;
    }
}
