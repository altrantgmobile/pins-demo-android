package nl.entreco.pinsandroid.detail;

import com.google.gson.annotations.SerializedName;

import android.text.TextUtils;

import nl.entreco.pinsandroid.utils.Utils;

/**
 * Created by 310185349 on 19-7-2016.
 */
public class DetailedResult {
    @SerializedName("photo")
    public PhotoDetail details;

    public String getOwnerName() {
        return TextUtils.isEmpty(details.owner.realname) ? Utils.EMPTY : details.owner.realname;
    }

    public String getDescription() {
        return details.description.content;
    }

    public String getDatePosted() {
        return Utils.timeStampToDateString(Long.parseLong(details.dates.posted));
    }
    public String getDateTaken() {
        return Utils.formattedStringToDateString(details.dates.taken);
    }
    public int getNumberOfViews(){
        return details.views;
    }
}
