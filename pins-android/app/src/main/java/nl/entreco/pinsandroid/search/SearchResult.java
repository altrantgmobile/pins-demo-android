package nl.entreco.pinsandroid.search;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 310185349 on 18-7-2016.
 */
public class SearchResult {
    @SerializedName("photos")
    public MetaData photos;

    public SearchResult() {
        photos = new MetaData();
    }

    boolean isEmpty() {
        return photos.photos.isEmpty();
    }

    public static class MetaData {
        @SerializedName("keyword")
        String keyword;
        @SerializedName("page")
        int page;
        @SerializedName("pages")
        int pages;
        @SerializedName("perpage")
        public int perpage;
        @SerializedName("total")
        public int total;
        @SerializedName("photo")
        public List<Photo> photos;

        public MetaData() {
            photos = new ArrayList<>();
        }
    }
}
