package nl.entreco.pinsandroid.api.interceptors;

import java.io.IOException;

import nl.entreco.pinsandroid.api.FlickrParams;
import nl.entreco.pinsandroid.api.FlickrRequestParams;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by 310185349 on 18-7-2016.
 */
public class FlickrParamsInterceptor implements Interceptor {
    private final FlickrParams params;

    public FlickrParamsInterceptor(FlickrParams _params) {
        this.params = _params;
    }

    @Override
    public Response intercept(final Chain chain) throws IOException {
        Request original = chain.request();
        HttpUrl originalHttpUrl = original.url();

        HttpUrl.Builder builder = originalHttpUrl.newBuilder();
        for (FlickrRequestParams.Param param : this.params.getParameters()) {
            builder.addQueryParameter(param.getName(), param.getValue());
        }

        // Request customization: add request headers
        Request.Builder requestBuilder = original.newBuilder()
                .url(builder.build());

        Request request = requestBuilder.build();
        return chain.proceed(request);
    }
}
