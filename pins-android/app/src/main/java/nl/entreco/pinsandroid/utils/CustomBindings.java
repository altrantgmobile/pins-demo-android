package nl.entreco.pinsandroid.utils;

import com.squareup.picasso.Picasso;

import android.animation.ObjectAnimator;
import android.databinding.BindingAdapter;
import android.os.Build;
import android.text.Html;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import nl.entreco.pinsandroid.R;

/**
 * Helper class to allow additional Binding methods.
 * Needs to be 'public' so Databinding library can find it
 * Created by 310185349 on 19-7-2016.
 */
@SuppressWarnings("WeakerAccess")
public final class CustomBindings {
    private CustomBindings() {
    }

    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView view, String imageUrl) {
        Picasso.with(view.getContext())
                .load(imageUrl)
                .placeholder(R.drawable.placeholder)
                .into(view);
    }

    @BindingAdapter({"numberOfLines"})
    public static void changeNumberOfLines(TextView _view, int _old, int _new) {
        if (_old != _new && _new > 0) {
            ObjectAnimator animator = ObjectAnimator.ofInt(_view, "maxLines", _new);
            animator.start();
        }
    }

    @BindingAdapter("html")
    public static void setTextWithHtmlTags(TextView _view, final String _text) {
        if (!TextUtils.isEmpty(_text)) {
            if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.N) {
                _view.setText(Html.fromHtml(_text));
            } else {
                _view.setText(Html.fromHtml(_text, Html.FROM_HTML_MODE_LEGACY));
            }
        }
    }
}
