package nl.entreco.pinsandroid.utils;

import trikita.log.Log;

/**
 * Created by 310185349 on 19-7-2016.
 */
final class Logger {
    private Logger() {
    }

    public static void v(Object msg, Object... args) {
        Log.v(msg, args);
    }

    public static void d(Object msg, Object... args) {
        Log.d(msg, args);
    }

    public static void i(Object msg, Object... args) {
        Log.i(msg, args);
    }

    public static void w(Object msg, Object... args) {
        Log.w(msg, args);
    }

    public static void e(Object msg, Object... args) {
        Log.e(msg, args);
    }

    public static void useFormat(final boolean _useFormat) {
        Log.useFormat(_useFormat);
        Log.level(-1);
    }
}
