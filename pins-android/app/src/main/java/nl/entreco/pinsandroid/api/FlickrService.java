package nl.entreco.pinsandroid.api;

import nl.entreco.pinsandroid.api.details.FlickrDetailedRequest;
import nl.entreco.pinsandroid.api.details.FlickrDetailedService;
import nl.entreco.pinsandroid.api.interceptors.DebugOnlyLogInterceptor;
import nl.entreco.pinsandroid.api.interceptors.FlickrParamsInterceptor;
import nl.entreco.pinsandroid.api.search.FlickrSearchRequest;
import nl.entreco.pinsandroid.api.search.FlickrSearchService;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by 310185349 on 19-7-2016.
 */
public interface FlickrService {

    class Factory {
        public static FlickrSearchService createSearchService() {
            Retrofit retrofit = create(createClient(new FlickrSearchRequest()));
            return retrofit.create(FlickrSearchService.class);
        }

        public static FlickrDetailedService createDetailsService() {
            Retrofit retrofit = create(createClient(new FlickrDetailedRequest()));
            return retrofit.create(FlickrDetailedService.class);
        }

        private static Retrofit create(OkHttpClient _client) {
            return new Retrofit.Builder()
                    .client(_client)
                    .baseUrl("https://api.flickr.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
        }

        private static OkHttpClient createClient(FlickrRequestParams _params) {
            final OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(new FlickrParamsInterceptor(_params));
            httpClient.addInterceptor(new DebugOnlyLogInterceptor());
            return httpClient.build();
        }
    }
}
