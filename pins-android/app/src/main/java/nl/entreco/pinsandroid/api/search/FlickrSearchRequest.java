package nl.entreco.pinsandroid.api.search;

import java.util.ArrayList;
import java.util.List;

import nl.entreco.pinsandroid.api.FlickrRequestParams;

/**
 * Created by 310185349 on 18-7-2016.
 */
public final class FlickrSearchRequest extends FlickrRequestParams {

    public static final List<Param> sParams = new ArrayList<Param>() {
        {
            add(new Param("method", SEARCH));
            add(new Param("api_key", API_KEY));
            add(new Param("content_type", CONTENT_TYPE));
            add(new Param("per_page", String.valueOf(PHOTOS_PER_PAGE)));
            add(new Param("format", "json"));
            add(new Param("nojsoncallback", "1"));
            add(new Param("privacy_filter", "1"));
        }
    };

    @Override
    public List<Param> getParameters() {
        return sParams;
    }
}
