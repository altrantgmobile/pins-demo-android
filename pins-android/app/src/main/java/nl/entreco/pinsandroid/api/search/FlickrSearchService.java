package nl.entreco.pinsandroid.api.search;

import nl.entreco.pinsandroid.search.SearchResult;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface FlickrSearchService {

    @GET("services/rest")
    Observable<SearchResult> performTextSearch(@Query("text") String text, @Query("page") int page);
}
