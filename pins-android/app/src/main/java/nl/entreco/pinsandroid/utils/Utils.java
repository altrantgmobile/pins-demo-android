package nl.entreco.pinsandroid.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import nl.entreco.pinsandroid.search.Photo;
import nl.entreco.pinsandroid.search.SearchActivity;

/**
 * Created by 310185349 on 19-7-2016.
 */
public class Utils {

    public static String EMPTY = "-";

    /** 2010-03-06 12:34:31 */
    private static final SimpleDateFormat sApiFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private static final int COLS_PORTAIT = 1;

    private Utils() {
    }

    /**
     * @param _timeStamp e.g. 1269886606
     */
    public static String timeStampToDateString(long _timeStamp) {
        Date date = new Date(_timeStamp * 1000L);
        return DateFormat.getDateInstance(DateFormat.DEFAULT).format(date);
    }

    /**
     * @param _formattedString e.g. 2010-03-06 12:34:31
     */
    public static String formattedStringToDateString(final String _formattedString) {
        if(_formattedString == null) return "";
        try {
            Date date = sApiFormat.parse(_formattedString);
            return DateFormat.getDateInstance(DateFormat.DEFAULT).format(date);
        } catch (ParseException _e) {
            Logger.e(_e);
        }
        return "";
    }

    public static void showKeyboard(final View _view) {
        InputMethodManager imm = (InputMethodManager) _view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }
    public static void hideKeyboard(final View _view) {
        InputMethodManager imm = (InputMethodManager) _view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(_view.getWindowToken(), 0);
    }

    public static boolean isConnected(final Context _context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String bigImageUrlForPhoto(final Photo _photo) {
        return String.format(Photo.BIG_IMAGE, _photo.farm, _photo.server, _photo.id, _photo.secret);
    }
    public static String thumbImageUrlForPhoto(final Photo _photo){
        return String.format(Photo.THUMB_IMAGE, _photo.farm, _photo.server, _photo.id, _photo.secret);
    }

    public static RecyclerView.LayoutManager orientationAwareGridLayoutManager(final Context _context) {
        return new GridLayoutManager(_context, getSpanCount(_context));
    }
    private static int getSpanCount(final Context _context){
        return isPortait(_context) ? COLS_PORTAIT : COLS_PORTAIT * 2;
    }

    private static boolean isPortait(final Context _context) {
        return _context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
    }

}
