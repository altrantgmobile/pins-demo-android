package nl.entreco.pinsandroid.search.items;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import nl.entreco.pinsandroid.R;
import nl.entreco.pinsandroid.api.FlickrRequestParams;
import nl.entreco.pinsandroid.databinding.ItemSearchBinding;
import nl.entreco.pinsandroid.search.Photo;
import nl.entreco.pinsandroid.search.SearchResult;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.PhotoViewHolder> implements SearchItemViewModel.DataListener {

    private static final int THRESHOLD = FlickrRequestParams.PHOTOS_PER_PAGE / 2;

    @Override
    public void onItemClicked(final View _view, final Photo _photo) {
        if (adapterCallback != null) {
            adapterCallback.onItemClick(_view, _photo);
        }
    }

    public interface AdapterCallback {
        void almostAtTheEnd();

        void onItemClick(final View _view, final Photo _photo);
    }

    private final List<Photo> photos;
    private final AdapterCallback adapterCallback;

    public SearchAdapter(AdapterCallback callback) {
        this.photos = new ArrayList<>();
        this.adapterCallback = callback;
    }

    public void addPhotos(SearchResult _searchResult) {
        if (_searchResult == null) {
            int end = this.photos.size();
            this.photos.clear();
            notifyItemRangeRemoved(0, end);
        } else {
            int start = this.photos.size() + 1;
            this.photos.addAll(_searchResult.photos.photos);
            notifyItemRangeInserted(start, _searchResult.photos.photos.size());
        }
    }

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemSearchBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_search,
                parent,
                false);
        return new PhotoViewHolder(binding, this);
    }

    @Override
    public void onBindViewHolder(PhotoViewHolder holder, int position) {
        holder.bindPhoto(photos.get(position));
        notifyActivityIfWereAlmostToTheEnd(position);
    }

    private void notifyActivityIfWereAlmostToTheEnd(int position) {
        if (photos.size() - position < THRESHOLD) {
            adapterCallback.almostAtTheEnd();
        }
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    static class PhotoViewHolder extends RecyclerView.ViewHolder {
        final ItemSearchBinding binding;
        final SearchItemViewModel.DataListener dataListener;

        PhotoViewHolder(ItemSearchBinding _binding, SearchItemViewModel.DataListener _dataListener) {
            super(_binding.cardView);
            this.binding = _binding;
            this.dataListener = _dataListener;
        }

        void bindPhoto(Photo _photo) {
            if (binding.getViewModel() == null) {
                binding.setViewModel(new SearchItemViewModel(itemView.getContext(), _photo, dataListener));
            } else {
                binding.getViewModel().setPhoto(_photo);
            }
        }
    }
}
