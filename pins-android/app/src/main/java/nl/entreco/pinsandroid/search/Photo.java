package nl.entreco.pinsandroid.search;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by 310185349 on 18-7-2016.
 */
@SuppressWarnings("CanBeFinal")
public class Photo implements Parcelable {
    public static final String BIG_IMAGE = "https://farm%s.staticflickr.com/%s/%s_%s_z.jpg";
    public static final String THUMB_IMAGE = "https://farm%s.staticflickr.com/%s/%s_%s_m.jpg";

    @SerializedName("id")
    public long id;
    @SerializedName("farm")
    public int farm;
    @SerializedName("owner")
    public String owner;
    @SerializedName("title")
    public String title;
    @SerializedName("server")
    public String server;
    @SerializedName("secret")
    public String secret;

    public Photo() {
        owner = "";
        title = "";
        server = "";
        secret = "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.owner);
        dest.writeString(this.title);
        dest.writeInt(this.farm);
        dest.writeString(this.server);
        dest.writeString(this.secret);
    }

    private Photo(Parcel in) {
        this.id = in.readLong();
        this.owner = in.readString();
        this.title = in.readString();
        this.farm = in.readInt();
        this.server = in.readString();
        this.secret = in.readString();
    }

    public static final Parcelable.Creator<Photo> CREATOR = new Creator<Photo>() {
        public Photo createFromParcel(Parcel source) {
            return new Photo(source);
        }

        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };
}
