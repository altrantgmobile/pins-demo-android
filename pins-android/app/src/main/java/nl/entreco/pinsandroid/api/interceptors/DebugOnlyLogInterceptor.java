package nl.entreco.pinsandroid.api.interceptors;

import java.io.IOException;

import nl.entreco.pinsandroid.BuildConfig;
import okhttp3.Interceptor;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Interceptor that only logs in Debug mode
 * Created by 310185349 on 19-7-2016.
 */
public class DebugOnlyLogInterceptor implements Interceptor {
    private final HttpLoggingInterceptor interceptor;

    public DebugOnlyLogInterceptor() {
        interceptor = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        }
    }

    @Override
    public Response intercept(final Chain chain) throws IOException {
        return interceptor.intercept(chain);
    }
}