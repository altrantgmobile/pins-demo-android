package nl.entreco.pinsandroid;

import android.app.Application;
import android.content.Context;

import nl.entreco.pinsandroid.api.FlickrService;
import nl.entreco.pinsandroid.api.details.FlickrDetailedService;
import nl.entreco.pinsandroid.api.search.FlickrSearchService;
import rx.Scheduler;
import rx.schedulers.Schedulers;

public class BaseApplication extends Application {

    private FlickrSearchService mFlickrSearchService;
    private FlickrDetailedService mFlickrDetailedService;
    private Scheduler defaultSubscribeScheduler;

    public static BaseApplication get(Context _context) {
        return (BaseApplication) _context.getApplicationContext();
    }

    public FlickrSearchService getFlickrSearchService() {
        if (mFlickrSearchService == null) {
            mFlickrSearchService = FlickrService.Factory.createSearchService();
        }
        return mFlickrSearchService;
    }

    //For setting mocks during testing
    public void setFlickrSearchService(FlickrSearchService _flickrSearchService) {
        this.mFlickrSearchService = _flickrSearchService;
    }

    public FlickrDetailedService getFlickrDetailedService() {
        if (mFlickrDetailedService == null) {
            mFlickrDetailedService = FlickrService.Factory.createDetailsService();
        }
        return mFlickrDetailedService;
    }

    //For setting mocks during testing
    public void setFlickrDetailedService(FlickrDetailedService _flickrDetailedService) {
        this.mFlickrDetailedService = _flickrDetailedService;
    }

    public Scheduler defaultSubscribeScheduler() {
        if (defaultSubscribeScheduler == null) {
            defaultSubscribeScheduler = Schedulers.io();
        }
        return defaultSubscribeScheduler;
    }

    //User to change scheduler from tests
    public void setDefaultSubscribeScheduler(Scheduler scheduler) {
        this.defaultSubscribeScheduler = scheduler;
    }
}
