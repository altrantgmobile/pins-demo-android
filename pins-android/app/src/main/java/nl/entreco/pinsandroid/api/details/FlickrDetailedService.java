package nl.entreco.pinsandroid.api.details;

import nl.entreco.pinsandroid.detail.DetailedResult;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by 310185349 on 18-7-2016.
 */
public interface FlickrDetailedService {

    @GET("services/rest")
    Observable<DetailedResult> getPhotoInfo(@Query("photo_id") String photoId);
}
