package nl.entreco.pinsandroid;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import nl.entreco.pinsandroid.api.search.FlickrSearchService;
import nl.entreco.pinsandroid.search.SearchResult;
import nl.entreco.pinsandroid.search.SearchViewModel;
import nl.entreco.pinsandroid.util.MockModelFabric;
import rx.Observable;
import rx.schedulers.Schedulers;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class SearchViewModelTest {

    FlickrSearchService mFlickrSearchService;
    BaseApplication application;
    SearchViewModel mSearchViewModel;
    SearchViewModel.DataListener dataListener;

    @Before
    public void setUp() {
        mFlickrSearchService = mock(FlickrSearchService.class);
        dataListener = mock(SearchViewModel.DataListener.class);
        application = (BaseApplication) RuntimeEnvironment.application;
        // Mock the retrofit service so we don't call the API directly
        application.setFlickrSearchService(mFlickrSearchService);
        // Change the default subscribe schedulers so all observables
        // will now run on the same thread
        application.setDefaultSubscribeScheduler(Schedulers.immediate());
        mSearchViewModel = new SearchViewModel(application, dataListener);
    }


    @Test
    public void shouldSearchUsernameWithRepos() {
        String username = "usernameWithRepos";
        TextView textView = new TextView(application);
        textView.setText(username);
        SearchResult mockRepos = MockModelFabric.newListOfRepositories(10);
        doReturn(rx.Observable.just(mockRepos)).when(mFlickrSearchService).performTextSearch(username, 0);

        mSearchViewModel.onSearchAction(textView, EditorInfo.IME_ACTION_SEARCH, null);
        verify(dataListener).onPhotosLoaded(mockRepos);
        assertEquals(mSearchViewModel.infoMessageVisibility.get(), View.INVISIBLE);
        assertEquals(mSearchViewModel.progressVisibility.get(), View.INVISIBLE);
        assertEquals(mSearchViewModel.recyclerViewVisibility.get(), View.VISIBLE);
    }

    @Test
    public void shouldSearchWithNoResults() {
        String searchQuery = "No results";
        TextView textView = new TextView(application);
        textView.setText(searchQuery);
        when(mFlickrSearchService.performTextSearch(searchQuery, 0))
                .thenReturn(Observable.just(new SearchResult()));

        mSearchViewModel.onSearchAction(textView, EditorInfo.IME_ACTION_SEARCH, null);
        verify(dataListener, times(2)).onPhotosLoaded(any(SearchResult.class));
        assertEquals(mSearchViewModel.infoMessage.get(),
                application.getString(R.string.no_photos_found, searchQuery));
        assertEquals(mSearchViewModel.infoMessageVisibility.get(), View.VISIBLE);
        assertEquals(mSearchViewModel.progressVisibility.get(), View.INVISIBLE);
        assertEquals(mSearchViewModel.recyclerViewVisibility.get(), View.INVISIBLE);


    }

}
