package nl.entreco.pinsandroid;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import android.view.View;

import java.io.IOException;

import nl.entreco.pinsandroid.api.details.FlickrDetailedService;
import nl.entreco.pinsandroid.detail.DetailedResult;
import nl.entreco.pinsandroid.detail.DetailedViewModel;
import nl.entreco.pinsandroid.search.Photo;
import nl.entreco.pinsandroid.util.MockModelFabric;
import nl.entreco.pinsandroid.utils.Utils;
import rx.Observable;
import rx.schedulers.Schedulers;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class DetailedViewModelTest {

    FlickrDetailedService mFlickrDetailedService;
    BaseApplication application;
    Photo photo;
    DetailedResult mDetailedResult;
    DetailedViewModel viewModel;

    @Before
    public void setUp() throws IOException {
        mFlickrDetailedService = mock(FlickrDetailedService.class);
        application = (BaseApplication) RuntimeEnvironment.application;
        // Mock the retrofit service so we don't call the API directly
        application.setFlickrDetailedService(mFlickrDetailedService);
        // Change the default subscribe schedulers so all observables
        // will now run on the same thread
        application.setDefaultSubscribeScheduler(Schedulers.immediate());
        // Default behaviour is to load a mock owner when the view model is instantiated
        photo = MockModelFabric.newPhoto("Remco");
        mDetailedResult = MockModelFabric.newPhotoInfo("photo_details.json");
        when(mFlickrDetailedService.getPhotoInfo(String.valueOf(photo.id)))
                .thenReturn(Observable.just(mDetailedResult));
        viewModel = new DetailedViewModel(application, photo, mock(DetailedViewModel.DataListener.class));
    }

    @Test
    public void shouldGetTitle() {
        assertEquals(photo.title, viewModel.getPhotoTitle());
    }

    @Test
    public void shouldGetBigImageUrl() {
        assertEquals(Utils.bigImageUrlForPhoto(photo), viewModel.getBigUrl());
    }

    @Test
    public void shouldLoadFullDetailsOnInstantiation() {
        assertEquals(mDetailedResult.getDescription(), viewModel.photoDescription.get());
        assertEquals(mDetailedResult.getDatePosted(), viewModel.datePosted.get());
        assertEquals(mDetailedResult.getOwnerName(), viewModel.ownerRealName.get());
        assertEquals(mDetailedResult.getDateTaken(), viewModel.dateTaken.get());
        assertEquals(mDetailedResult.getNumberOfViews(), viewModel.numberOfViews.get());
        assertEquals(View.VISIBLE, viewModel.hasDescription.get());
    }
}
