package nl.entreco.pinsandroid;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import android.content.Context;
import android.content.Intent;
import android.databinding.Observable;
import android.view.View;

import nl.entreco.pinsandroid.search.Photo;
import nl.entreco.pinsandroid.search.items.SearchItemViewModel;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class SearchItemViewModelTest {

    BaseApplication application;

    @Before
    public void setUp() {
        application = (BaseApplication) RuntimeEnvironment.application;
    }

    @Test
    public void shouldGetName() {
        Photo photo = new Photo();
        photo.title = "Photo title";
        SearchItemViewModel searchItemViewModel = new SearchItemViewModel(application, photo, mock(SearchItemViewModel.DataListener.class));
        assertEquals(photo.title, searchItemViewModel.getTitle());
    }

    @Test
    public void shouldStartActivityOnItemClick() {
        Photo photo = new Photo();
        View view = new View(application);
        Context mockContext = mock(Context.class);
        SearchItemViewModel.DataListener mockDataListener = mock(SearchItemViewModel.DataListener.class);
        SearchItemViewModel searchItemViewModel = new SearchItemViewModel(mockContext, photo,mockDataListener);
        searchItemViewModel.onItemClick(view);
        verify(mockDataListener).onItemClicked(view, photo);
    }

    @Test
    public void shouldNotifyPropertyChangeWhenSetRepository() {
        Photo photo = new Photo();
        SearchItemViewModel searchItemViewModel = new SearchItemViewModel(application, photo, mock(SearchItemViewModel.DataListener.class));
        Observable.OnPropertyChangedCallback mockCallback =
                mock(Observable.OnPropertyChangedCallback.class);
        searchItemViewModel.addOnPropertyChangedCallback(mockCallback);

        searchItemViewModel.setPhoto(photo);
        verify(mockCallback).onPropertyChanged(any(Observable.class), anyInt());
    }
}
