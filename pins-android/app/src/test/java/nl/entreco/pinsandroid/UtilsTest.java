package nl.entreco.pinsandroid;

import org.junit.Test;

import nl.entreco.pinsandroid.utils.Utils;

import static org.junit.Assert.assertEquals;

/**
 * Created by 310185349 on 19-7-2016.
 */
public class UtilsTest {

    @Test
    public void shouldConvertDate() throws Exception {
        assertEquals("1-jan-1970", Utils.timeStampToDateString(0));
    }

    @Test
    public void shouldReturnEmptyStringForInvalidInput() throws Exception {
        assertEquals("",Utils.formattedStringToDateString("remco"));
        assertEquals("",Utils.formattedStringToDateString(null));
        assertEquals("",Utils.formattedStringToDateString("dddd-mm-yyyy"));
    }
}
