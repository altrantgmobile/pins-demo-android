package nl.entreco.pinsandroid.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import nl.entreco.pinsandroid.detail.DetailedResult;

/**
 * Created by 310185349 on 19-7-2016.
 */
public class FileParser {

    public static final String PATH_TO_TEST_RESOURCES = "src/test/resources/";
    private static final String UTF8 = "UTF8";

    public static DetailedResult getPhotoInfoFromFile(final String _file) throws IOException {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(getReader(_file), DetailedResult.class);
    }

    private static JsonReader getReader(final String _file) throws FileNotFoundException, UnsupportedEncodingException {
        return getReader(PATH_TO_TEST_RESOURCES, _file);
    }

    public static JsonReader getReader(String _prefix, String _file)
            throws FileNotFoundException, UnsupportedEncodingException {
        return new JsonReader(
                new InputStreamReader(new FileInputStream(
                        _prefix + _file),
                        UTF8));
    }
}
