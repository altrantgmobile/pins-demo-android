package nl.entreco.pinsandroid.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import nl.entreco.pinsandroid.detail.DetailedResult;
import nl.entreco.pinsandroid.search.Photo;
import nl.entreco.pinsandroid.search.SearchResult;

public class MockModelFabric {

    public static SearchResult newListOfRepositories(int numRepos) {
        SearchResult searchResult = new SearchResult();
        searchResult.photos = new SearchResult.MetaData();
        List<Photo> repositories = new ArrayList<>(numRepos);
        for (int i = 0; i < numRepos; i++) {
            repositories.add(newPhoto("Repo " + i));
        }
        searchResult.photos.photos = repositories;
        return searchResult;
    }

    public static Photo newPhoto(String name) {
        Random random = new Random();
        Photo photo = new Photo();
        photo.title = name;
        photo.id = random.nextInt(10000);
        photo.owner = "owner";
        return photo;
    }

    public static DetailedResult newPhotoInfo(String fromFile) throws IOException {
        return FileParser.getPhotoInfoFromFile(fromFile);
    }
}
