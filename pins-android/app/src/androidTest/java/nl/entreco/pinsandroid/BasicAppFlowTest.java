package nl.entreco.pinsandroid;


import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import android.support.test.espresso.ViewInteraction;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import nl.entreco.pinsandroid.R;
import nl.entreco.pinsandroid.search.SearchActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class BasicAppFlowTest {

    @Rule
    public ActivityTestRule<SearchActivity> mActivityTestRule = new ActivityTestRule<>(SearchActivity.class);

    @Test
    public void basicAppFlowTest() {
        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.searchQueryInput), isDisplayed()));
        appCompatEditText.perform(click());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.searchQueryInput), isDisplayed()));
        appCompatEditText2.perform(replaceText("pikachu"));

        ViewInteraction appCompatImageButton = onView(
                allOf(withId(R.id.button_search), withContentDescription("Search images"), isDisplayed()));
        appCompatImageButton.perform(click());

        ViewInteraction relativeLayout = onView(
                allOf(withId(R.id.layout_content),
                        withParent(allOf(withId(R.id.card_view),
                                withParent(withId(R.id.recycler_view)))),
                        isDisplayed()));
        relativeLayout.perform(click());

        ViewInteraction imageButton = onView(
                allOf(withContentDescription("Omhoog navigeren"),
                        withParent(allOf(withId(R.id.toolbar),
                                withParent(withId(R.id.collapsing_toolbar)))),
                        isDisplayed()));
        imageButton.perform(click());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.searchQueryInput), withText("pikachu"), isDisplayed()));
        appCompatEditText3.perform(replaceText("cool"));

        ViewInteraction appCompatImageButton2 = onView(
                allOf(withId(R.id.button_search), withContentDescription("Search images"), isDisplayed()));
        appCompatImageButton2.perform(click());

    }

}
