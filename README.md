# PINS Demo
One of the most important aspects of Agile, is completing the Sprint on time.
As such, some really nice features (offline behaviour,  advanced search/filter, swipe through search results) did not make it into this sample application.
I do feel, however, that this first release has a basis which can be build upon.

***I would be happy to discuss some of the choices made to complete this demo***






![screenshots.png](https://bitbucket.org/repo/7Br4Xj/images/2764430629-screenshots.png)

App is (temporarily) available on [Google Play](https://play.google.com/store/apps/details?id=nl.entreco.pinsandroid)
for Android 5.0	Lollipop devices and up!


## Relevant Information

* [Build](../../wiki/Build)
* [Libraries](../../wiki/Libraries)
* [Architecture](../../wiki/Architecture)
* [Code Quality](../../wiki/CodeQuality)
* [Testing](../../wiki/Testing)
* [UI/UX](../../wiki/UiUx)